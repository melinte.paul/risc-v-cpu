`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 09:12:29 PM
// Design Name: 
// Module Name: ALU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ALU(input [3:0] ALUop,
           input [31:0] ina,inb,
           output reg zero,
           output reg [31:0] out);
           
    always@(*)
    begin
        if(ALUop == 4'b0000)
        begin
            out = ina & inb;
        end
        if(ALUop == 4'b0001)
        begin
            out = ina | inb;
        end
        if(ALUop == 4'b0010)
        begin
            out = ina + inb;
        end
        if(ALUop == 4'b0110)
        begin
            out = ina - inb;
        end

        if(out == 32'b0)
        begin
            zero = 1;
        end else zero = 0;
    end          

endmodule
