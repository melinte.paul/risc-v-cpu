`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 06:20:19 PM
// Design Name: 
// Module Name: control_path
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control_path(input [6:0] opcode,
                    input control_sel,
                    output reg MemRead,MemtoReg,MemWrite,RegWrite,Branch,ALUSrc,
                    output reg [1:0] ALUop);
                    
    always@(*)
    begin
        if(opcode == 7'b0110011) //R-TYPE
            begin
                MemRead <= 0;
                MemtoReg <= 0;
                MemWrite <= 0;
                RegWrite <= 1;
                Branch <= 0;
                ALUSrc <= 0;
                ALUop <= 2'b10;
            end
        if(opcode == 7'b0010011) //I-TYPE
            begin
                MemRead <= 0;
                MemtoReg <= 0;
                MemWrite <= 0;
                RegWrite <= 1;
                Branch <= 0;
                ALUSrc <= 1;
                ALUop <= 2'b10;
            end
        if(opcode == 7'b0000011) //LOAD
            begin
                MemRead <= 1;
                MemtoReg <= 1;
                MemWrite <= 0;
                RegWrite <= 1;
                Branch <= 0;
                ALUSrc <= 1;
                ALUop <= 2'b00;
            end
        if(opcode == 7'b0100011) //STORE
            begin
                MemRead <= 0;
                MemtoReg <= 0;
                MemWrite <= 1;
                RegWrite <= 0;
                Branch <= 0;
                ALUSrc <= 1;
                ALUop <= 2'b00;
            end
        if(opcode == 7'b1100011) //BEQ
            begin
                MemRead <= 0;
                MemtoReg <= 0;
                MemWrite <= 0;
                RegWrite <= 0;
                Branch <= 1;
                ALUSrc <= 0;
                ALUop <= 2'b01;
            end
        if(control_sel == 0 || opcode == 7'b0)
        begin
            MemRead <= 0;
            MemtoReg <= 0;
            MemWrite <= 0;
            RegWrite <= 0;
            Branch <= 0;
            ALUSrc <= 0;
            ALUop <= 2'b00;
        end
    end
endmodule
