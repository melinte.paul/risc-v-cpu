`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 11:26:48 PM
// Design Name: 
// Module Name: risc_v_final
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RISC_V(input clk,
              input reset,
              
              output wire [31:0] PC_EX,
              output wire [31:0] ALU_OUT_EX,
              output wire [31:0] PC_MEM,
              output reg PCSrc,
              output wire [31:0] DATA_MEMORY_MEM,
              output wire [31:0] ALU_DATA_WB,
              output wire [1:0] forwardA, forwardB,
              output wire pipeline_stall
             );
      
      //IF_ID       
      wire IF_ID_write;
      wire PC_write;
      wire [31:0] PC_Branch;
      
      wire RegWrite_WB;     
      wire [4:0] RD_WB;
      
      wire [31:0] PC_ID; 
      wire [31:0] INSTRUCTION_ID;
      wire [31:0] IMM_ID;        
      wire [31:0] REG_DATA1_ID;  
      wire [31:0] REG_DATA2_ID;  
      wire [2:0] FUNCT3_ID; 
      wire [6:0] FUNCT7_ID; 
      wire [6:0] OPCODE_ID; 
      wire [4:0] RD_ID;     
      wire [4:0] RS1_ID;    
      wire [4:0] RS2_ID;    
    initial begin
        PCSrc = 0;
    end
    RISC_V_IF_ID if_id(clk,reset,IF_ID_write,PCSrc,PC_write,PC_MEM,
        RegWrite_WB,ALU_DATA_WB,RD_WB,
        PC_ID,INSTRUCTION_ID,IMM_ID,REG_DATA1_ID,REG_DATA2_ID,
        FUNCT3_ID,FUNCT7_ID,OPCODE_ID,RD_ID,RS1_ID,RS2_ID);
    
    wire [4:0] RD_EX;
    wire MemRead_EX; 
    wire control_sel;
    
    hazard_detection HAZARD_DETECTION(RD_EX,RS1_ID,RS2_ID,MemRead_EX,
        PC_write,IF_ID_write,control_sel);
    
    assign pipeline_stall = !control_sel;
    
    wire MemRead_ID,MemtoReg_ID,MemWrite_ID,RegWrite_ID,Branch_ID,ALUSrc_ID;
    wire [1:0] ALUop_ID;
    control_path CONTROL(OPCODE_ID,control_sel,
        MemRead_ID,MemtoReg_ID,MemWrite_ID,RegWrite_ID,Branch_ID,ALUSrc_ID,
        ALUop_ID);
    
    //register
    wire [31:0] IMM_EX,REG_DATA1_EX,REG_DATA2_EX;
    wire RegWrite_EX,MemtoReg_EX,MemWrite_EX,ALUSrc_EX,Branch_EX; 
    wire [1:0] ALUop_EX;
    wire [2:0] FUNCT3_EX; 
    wire [6:0] FUNCT7_EX; 
    wire [4:0] RS1_EX,RS2_EX;
    
    ID_EX_reg ID_EX_REG(clk,1'b1,reset,IMM_ID,REG_DATA1_ID,REG_DATA2_ID,PC_ID,
        IMM_EX,REG_DATA1_EX,REG_DATA2_EX,PC_EX,
        
        RegWrite_ID,MemtoReg_ID,MemRead_ID,MemWrite_ID,ALUSrc_ID,Branch_ID,
        RegWrite_EX,MemtoReg_EX,MemRead_EX,MemWrite_EX,ALUSrc_EX,Branch_EX,
        
        ALUop_ID,ALUop_EX,FUNCT3_ID,FUNCT3_EX,FUNCT7_ID,FUNCT7_EX,
        
        RD_ID,RS1_ID,RS2_ID,RD_EX,RS1_EX,RS2_EX);
    
    //EX
    wire ZERO_EX;
    wire [31:0] PB_Branch_EX,REG_DATA2_EX_FINAL,ALU_OUT_MEM,ALU_OUT_WB;
    
    EX ex(IMM_EX,REG_DATA1_EX,REG_DATA2_EX,PC_EX,FUNCT3_EX,FUNCT7_EX,RD_EX,RS1_EX,RS2_EX,
        RegWrite_EX,MemtoReg_EX,MemRead_EX,MemWrite_EX,
        ALUop_EX,ALUSrc_EX,Branch_EX,forwardA,forwardB,ALU_OUT_WB,ALU_OUT_MEM,
        ZERO_EX,ALU_OUT_EX,PB_Branch_EX,REG_DATA2_EX_FINAL);
    
    wire [4:0] RD_MEM;
    wire RegWrite_MEM;
        
    forwarding FORWARDING(RS1_EX,RS2_EX,RD_MEM,RD_WB,RegWrite_MEM,RegWrite_WB,forwardA,forwardB);    
        
    //register                       
    wire Branch_MEM,MemRead_MEM,MemWrite_MEM,MemtoReg_MEM,ZERO_MEM;
    wire [31:0] REG_DATA2_MEM;
    
    EX_MEM_reg EX_MEM_REG(clk,1,reset,Branch_EX,MemRead_EX,MemWrite_EX,MemtoReg_EX,ZERO_EX,RegWrite_EX,
        Branch_MEM,MemRead_MEM,MemWrite_MEM,MemtoReg_MEM,ZERO_MEM,RegWrite_MEM,
        
        RD_EX,RD_MEM,
        
        ALU_OUT_EX,REG_DATA2_EX_FINAL,PB_Branch_EX,
        ALU_OUT_MEM,REG_DATA2_MEM,PC_MEM);
        
    //MEM
    
    assign PCsrc = Branch_MEM & ZERO_MEM;
        
    data_memory DATA_MEMORY(clk,MemRead_MEM,MemWrite_MEM,ALU_OUT_MEM,REG_DATA2_MEM,DATA_MEMORY_MEM);
    
    //register
    
    wire MemtoReg_WB;
    wire [31:0] DATA_MEMORY_WB;
    
    MEM_WB_reg MEM_WB_REG(clk,1,reset,MemtoReg_MEM,RegWrite_MEM,MemtoReg_WB,RegWrite_WB,
        DATA_MEMORY_MEM,ALU_OUT_MEM,DATA_MEMORY_WB,ALU_OUT_WB,
        RD_MEM,RD_WB);
    
    //WB
        
    mux2_1 MemToRegMux(ALU_OUT_WB,DATA_MEMORY_WB,MemtoReg_WB,ALU_DATA_WB);
               
endmodule
