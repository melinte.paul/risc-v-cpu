`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 11:04:18 PM
// Design Name: 
// Module Name: EX_MEM_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX_MEM_reg(
        input clk,write,reset,
        input Branch_EX,MemRead_EX,MemWrite_EX,MemtoReg_EX,ZERO_EX,RegWrite_EX,
        output reg Branch_MEM,MemRead_MEM,MemWrite_MEM,MemtoReg_MEM,ZERO_MEM,RegWrite_MEM,
        input [4:0] RD_EX,
        output reg [4:0] RD_MEM,
        input [31:0] ALU_OUT_EX,REG_DATA2_EX,PC_EX,
        output reg [31:0] ALU_OUT_MEM,REG_DATA2_MEM,PC_MEM
    );
    
    always@(posedge clk) begin
        if (reset) begin
        Branch_MEM <= 0;
        MemRead_MEM <= 0;
        MemWrite_MEM <= 0;
        MemtoReg_MEM <= 0;
        RegWrite_MEM <= 0;
        ZERO_MEM <= 0;
        RD_MEM <= 0;
        ALU_OUT_MEM <= 0;
        REG_DATA2_MEM <= 0;
        PC_MEM <= 0;
        end 
        else if(write) begin
        Branch_MEM <= Branch_EX;
        MemRead_MEM <= MemRead_EX;
        MemWrite_MEM <= MemWrite_EX;
        MemtoReg_MEM <= MemtoReg_EX;
        RegWrite_MEM <= RegWrite_EX;
        ZERO_MEM <= ZERO_EX;
        RD_MEM <= RD_EX;
        ALU_OUT_MEM <= ALU_OUT_EX;
        REG_DATA2_MEM <= REG_DATA2_EX;
        PC_MEM <= PC_EX;
        end
    end
endmodule
