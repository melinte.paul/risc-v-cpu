`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 11:04:18 PM
// Design Name: 
// Module Name: MEM_WB_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MEM_WB_reg(input clk,write,reset,
    input MemtoReg_MEM,RegWrite_MEM,
    output reg MemtoReg_WB,RegWrite_WB,
    input [31:0] DATA_MEMORY_MEM,ALU_OUT_MEM,
    output reg [31:0] DATA_MEMORY_WB,ALU_OUT_WB,
    input [4:0] RD_MEM,
    output reg [4:0] RD_WB
    );
    
     always@(posedge clk) begin
        if (reset) begin
        MemtoReg_WB <= 0;
        RegWrite_WB <= 0;
        DATA_MEMORY_WB <= 0;
        ALU_OUT_WB <= 0;
        RD_WB <= 0;
        end 
        else if(write) begin
        MemtoReg_WB <= MemtoReg_MEM;
        RegWrite_WB <= RegWrite_MEM;
        DATA_MEMORY_WB <= DATA_MEMORY_MEM;
        ALU_OUT_WB <= ALU_OUT_MEM;
        RD_WB <= RD_MEM;
        end
    end
endmodule
