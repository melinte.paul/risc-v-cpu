`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 08:33:56 PM
// Design Name: 
// Module Name: ALUcontrol
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module ALUcontrol(input [1:0] ALUop,
                  input [6:0] funct7,
                  input [2:0] funct3,
                  output reg [3:0] ALUinput);
    always@(*)
    begin
       if(ALUop == 2'b00)
       begin 
           ALUinput = 4'b0010;
       end else 
       if(ALUop == 2'b01)
       begin
           ALUinput = 4'b0110;
       end else    
       if(ALUop == 2'b10 && funct7 == 7'b0000000 && funct3 == 3'b000)
       begin
           ALUinput = 4'b0010;
       end else    
       if(ALUop == 2'b10 && funct7 == 7'b0100000 && funct3 == 3'b000)
       begin
           ALUinput = 4'b0110;
       end else    
       if(ALUop == 2'b10 && funct7 == 7'b0000000 && funct3 == 3'b111)
       begin
           ALUinput = 4'b0000;
       end else    
       if(ALUop == 2'b10 && funct7 == 7'b0000000 && funct3 == 3'b110)
       begin
           ALUinput = 4'b0001;
       end   
    end  
endmodule
