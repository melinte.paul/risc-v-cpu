`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 10:31:40 PM
// Design Name: 
// Module Name: data_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module data_memory(input clk,       
                   input mem_read,
                   input mem_write,
                   input [31:0] address,
                   input [31:0] write_data,
                   output reg [31:0] read_data
                   );
    
    wire [9:0] address_norm = address[11:2];
                   
    reg [31:0] dataMemory [0:1024];
    
    integer i;
    initial begin
       for (i = 0; i < 1024; i = i + 1) begin
         dataMemory[i] = 32'b0;
       end
     end
  
    always@(mem_read)
    begin 
        if(mem_read)
            read_data = dataMemory[address_norm];
    end
  
    always@(posedge clk)
    begin
        if(mem_write)
            begin
                dataMemory[address_norm] <= write_data;
            end
    end

endmodule
