`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 11:04:18 PM
// Design Name: 
// Module Name: ID_EX_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ID_EX_reg(
    input clk,write,reset,
    input [31:0] imm_id,reg_data1_id,reg_data2_id,pc_id,
    output reg [31:0] imm_ex,reg_data1_ex,reg_data2_ex,pc_ex,
    input regwrite_id,memtoreg_id,memread_id,memwrite_id,alusrc_id,branch_id,
    output reg regwrite_ex,memtoreg_ex,memread_ex,memwrite_ex,alusrc_ex,branch_ex,
    input [1:0] aluop_id,
    output reg [1:0] aluop_ex,
    input [2:0] funct3_id,
    output reg [2:0] funct3_ex,
    input [6:0] funct7_id,
    output reg [6:0] funct7_ex,
    input [4:0] rd_id,rs1_id,rs2_id,
    output reg [4:0] rd_ex,rs1_ex,rs2_ex
    );
    
    always@(posedge clk) begin
    if (reset) begin
        imm_ex <= 32'b0;
        reg_data1_ex <= 32'b0;
        reg_data2_ex <= 32'b0;
        pc_ex <= 32'b0;
        regwrite_ex <= 0;
        memtoreg_ex <= 0;
        memread_ex <= 0;
        memwrite_ex <= 0;
        alusrc_ex <= 0;
        branch_ex <= 0;
        aluop_ex <= 2'b0;
        funct3_ex <= 32'b0;
        funct7_ex <= 7'b0;
        rd_ex <= 5'b0;
        rs1_ex <= 5'b0;
        rs2_ex <= 5'b0;
    end
    else begin
      if(write) begin
        imm_ex <= imm_id;
        reg_data1_ex <= reg_data1_id;
        reg_data2_ex <= reg_data2_id;
        pc_ex <= pc_id;
        regwrite_ex <= regwrite_id;
        memtoreg_ex <= memtoreg_id;
        memread_ex <= memread_id;
        memwrite_ex <= memwrite_id;
        alusrc_ex <= alusrc_id;
        branch_ex <= branch_id;
        aluop_ex <= aluop_id;
        funct3_ex <= funct3_id;
        funct7_ex <= funct7_id;
        rd_ex <= rd_id;
        rs1_ex <= rs1_id;
        rs2_ex <= rs2_id;

      end
    end
  end
endmodule
