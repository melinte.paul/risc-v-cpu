`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/28/2020 09:33:13 PM
// Design Name: 
// Module Name: EX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX(input [31:0] IMM_EX,         
          input [31:0] REG_DATA1_EX,
          input [31:0] REG_DATA2_EX,
          input [31:0] PC_EX,
          input [2:0] FUNCT3_EX,
          input [6:0] FUNCT7_EX,
          input [4:0] RD_EX,
          input [4:0] RS1_EX,
          input [4:0] RS2_EX,
          input RegWrite_EX,
          input MemtoReg_EX,
          input MemRead_EX,
          input MemWrite_EX,
          input [1:0] ALUop_EX,
          input ALUSrc_EX,
          input Branch_EX,
          input [1:0] forwardA,forwardB,
          
          input [31:0] ALU_DATA_WB,
          input [31:0] ALU_OUT_MEM,
          
          output ZERO_EX,
          output [31:0] ALU_OUT_EX,
          output [31:0] PC_Branch_EX,
          output [31:0] REG_DATA2_EX_FINAL
          );
    
    wire [31:0] RS1_ALU;
          
    mux4_1 muxrs1(REG_DATA1_EX,ALU_DATA_WB,ALU_OUT_MEM,32'b0,forwardA, RS1_ALU);         
    mux4_1 muxrs2prelim(REG_DATA2_EX,ALU_DATA_WB,ALU_OUT_MEM,32'b0,forwardB, REG_DATA2_EX_FINAL);         
    
    wire [31:0] RS2_ALU;

    mux2_1 muxrs2(REG_DATA2_EX_FINAL,IMM_EX,ALUSrc_EX,RS2_ALU);
    
    adder PC_IMM_ADDER(PC_EX,IMM_EX,PC_Branch_EX);
    
    wire [3:0] ALUinput;
    
    ALUcontrol ALU_CONTROL(ALUop_EX,FUNCT7_EX,FUNCT3_EX,ALUinput);
    
    ALU alu(ALUinput,RS1_ALU,RS2_ALU,ZERO_EX,ALU_OUT_EX);      
endmodule
